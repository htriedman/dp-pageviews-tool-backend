import gzip
import json
import logging
import math
import os
import re
import requests
import sqlite3


from flask import Flask, request, jsonify, make_response
from flask_cors import CORS
import yaml

app = Flask(__name__)

__dir__ = os.path.dirname(__file__)

# load in app user-agent or any other app config
app.config.update(
    yaml.safe_load(open(os.path.join(__dir__, 'flask_config.yaml'))))
app.json.sort_keys = False

# Enable CORS for API endpoints
cors = CORS(app, resources={r'/*': {'origins': '*'}})

DB_PATH = "/etc/api-endpoint/resources/dpviews.db"
# DB_PATH = os.path.abspath(os.path.join(os.pardir,"resources","dpviews.db"))
# print("Try: http://127.0.0.1:5000/api/v1/en.wikipedia/42316753/2024-01-02")

@app.route('/')
def index():
    return 'Server Works!'

@app.route("/api/v1/meta")
def get_latest_metadata():
    conn = sqlite3.connect(DB_PATH)
    cursor = conn.cursor()
    query = f"SELECT MIN(date), MAX(date) FROM dpv"
    results = cursor.execute(query).fetchall()
    earliest_day, latest_day = results[0]
    return {"earliest_day": earliest_day, "latest_day": latest_day}


@app.route(
    "/api/v1/<project>/<page_id>/<date>"
)
def serve_query(project, page_id, date):
    data = read_data(project, page_id, date)
    return construct_response(data)

def construct_response(data):
    res = make_response({
        "results": [{
            "country": d[0],
            "country_code": d[1],
            "project": d[2],
            "page_id": d[3],
            "page_title": d[4],
            "wikidata_id": d[5],
            "noisy_views": d[6],
            "date": d[7]
        } for d in data]
    })
    return res

def read_data(project, page_id, date):
    conn = sqlite3.connect(DB_PATH)
    cursor = conn.cursor()
    query = "SELECT * FROM dpv WHERE project=? AND page_id=? AND date=? ORDER BY noisy_views DESC"
    results = cursor.execute(query, (project, page_id, date)).fetchall()
    return results

application = app

if __name__ == '__main__':
    application.run()
