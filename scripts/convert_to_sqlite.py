import argparse
import pandas as pd
import os
import sqlite3
import datetime

BASE_URL = "https://analytics.wikimedia.org/published/datasets/country_project_page"

def convert(date_start_obj, date_end_obj, sqlite_db, table_name):

    print("Creating SQLite database...")
    conn = sqlite3.connect(sqlite_db, isolation_level=None)

    # disable the rollback journal
    conn.execute("PRAGMA journal_mode = OFF;")
    # disable database disk synchronization
    conn.execute("PRAGMA synchronous = 0;")
    # don't release file locks after every transaction
    conn.execute("PRAGMA locking_mode = EXCLUSIVE;")

    print("Loading data into table...")

    # iterating over all data files
    # getting list of days
    delta = datetime.timedelta(days=1)
    day = date_start_obj
    list_days = []
    while day <= date_end_obj:
        list_days+=[day]
        day += delta

    # loading and adding data from each
    for date in list_days:
        date_str = date.strftime("%Y-%m-%d")
        URL = "{0}/{1}.tsv".format(BASE_URL,date_str)
        df = load_csv(URL)
        df["date"] = date_str
        df.to_sql(table_name, conn, if_exists="append", index=False)
        print(f"Completed {date}")

    print("Creating index on col=project")
    conn.execute("CREATE INDEX IDX_COUNTRY on dpv(country_code)")

    print("Creating index on col=country_code")
    conn.execute("CREATE INDEX IDX_PROJECT on dpv(project)")

    print("Creating index on col=page_id")
    conn.execute("CREATE INDEX IDX_PAGE_ID on dpv(page_id)")

    print("Creating index on col=date")
    conn.execute("CREATE INDEX IDX_DATE on dpv(date)")

    conn.close()
    print(f"The SQLite file {sqlite_db} has been created.")


def load_csv(dump_file):
    return pd.read_csv(
        dump_file,
        sep="\t",
        header=None, names = ["country","country_code","project","page_id","page_title","item_id","noisy_views"],
        quoting=3,
        encoding="utf8",
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Convert the dp-pageviews data to a SQLite database."
    )

    parser.add_argument(
        "--date_start","-d1", type=str, help="First day of data. Use format YYYY-MM-DD."
    )
    parser.add_argument(
        "--date_end", "-d2", type=str, help="Last day of data (inclusive). Use format YYYY-MM-DD."
    )

    parser.add_argument(
        "--sqlite_db",
        type=str,
        nargs="?",
        help="Output SQLite database file. Use format xyz.db.",
        default="dpviews.db",
    )
    parser.add_argument(
        "table_name",
        type=str,
        nargs="?",
        help="Name of the table to write to in the SQLite \
                        file.",
        default="dpv",
    )

    args = parser.parse_args()

    date_start_obj = datetime.datetime.strptime(args.date_start, '%Y-%m-%d').date()
    date_end_obj = datetime.datetime.strptime(args.date_end, '%Y-%m-%d').date()

    sqlite_db = args.sqlite_db
    table_name = args.table_name

    convert(date_start_obj, date_end_obj, sqlite_db, table_name)