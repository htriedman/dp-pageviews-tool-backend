# DP-Pageviews backend API endpoint

This repo provides the code for setting up an endpoint on [Cloud-VPS](https://wikitech.wikimedia.org/wiki/Help:Cloud_VPS) for querying the [differentially-private daily pageview dataset](https://analytics.wikimedia.org/published/datasets/country_project_page/00_README.html).

Once set up, you can run queries in the following format:

`https://dp-pageviews.wmcloud.org/api/v1/<project>/<page_id>/<date>`

For example:

`https://dp-pageviews.wmcloud.org/api/v1/en.wikipedia/38375/2024-04-02`


This will give you the number of (noisy) pageviews from each country to the page in the project on the selected date.

## Content

* `model/config/` contains the code for setting up the instance on Cloud-VPS.
* `scripts/` contains the code for converting the [publicly available data](https://analytics.wikimedia.org/published/datasets/country_project_page/) into an SQLite database.
* `model/wsgi.py` contains the code for generating the responses.


### Acknowledgements
Adapted from https://github.com/wikimedia/research-api-endpoint-template
